package run.bottle.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author liyc
 * @date 2022-04-10
 */
public class ServiceException extends AbstractBottleException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
