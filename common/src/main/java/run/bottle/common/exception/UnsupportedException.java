package run.bottle.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author liyc
 * @date 2022-05-06
 */
public class UnsupportedException extends AbstractBottleException {
    public UnsupportedException(String message) {
        super(message);
    }

    public UnsupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.UNAUTHORIZED;
    }
}
