package run.bottle.common.exception;

/**
 * @author liyc
 * @date 2022-04-12
 */
public class UsernameNotFoundException extends ServiceException{

    public UsernameNotFoundException(String message) {
        super(message);
    }

    public UsernameNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
