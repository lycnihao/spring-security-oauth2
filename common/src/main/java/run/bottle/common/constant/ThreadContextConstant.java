package run.bottle.common.constant;

/**
 * @author liyc
 * @date 2022-04-05
 */
public interface ThreadContextConstant {

    String CURRENT_USER = "currentUser";
    String USERNAME = "username";
    String USER_ID = "userId";
    String ACCESS_TOKEN = "accessToken";

}
