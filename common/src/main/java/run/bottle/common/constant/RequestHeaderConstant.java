package run.bottle.common.constant;

/**
 * @author liyc
 * @date 2022-04-10
 */
public class RequestHeaderConstant {

    public static final String AUTHORIZATION = "Authorization";
    public static final String CLIENT_ID = "x-client-id";

}
