package run.bottle.common.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import run.bottle.common.constant.ThreadContextConstant;
import run.bottle.common.entity.AppUser;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liyc
 * @date 2022-04-05
 */
public class ThreadContext {

    private static Logger logger = LoggerFactory.getLogger(ThreadContext.class);

    private static ThreadLocal<Map<String, Object>> resources = new ThreadLocal<>();

    /**
     * Bind given key and given value to current thread
     *
     * @param key
     * @param value
     */
    public static void set(String key, Object value) {

        Map<String, Object> resourceMap = resources.get();

        if (resourceMap == null) {
            resourceMap = new HashMap<>();
            resources.set(resourceMap);
        }

        if (resourceMap.containsKey(key)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Override exists resource({}) in thread context.", key);
            }
        }

        resourceMap.put(key, value);
        if (logger.isDebugEnabled()) {
            logger.debug("Bind resource to thread context successfully, [key={},value={}]", key, value);
        }
    }

    /**
     * Retrieve resource for the given key from current thread
     *
     * @param key
     * @return
     */
    public static Object get(String key) {

        Object value = null;
        Map<String, Object> resourceMap = resources.get();

        if (resourceMap == null || resourceMap.size() == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("The resource is already unbind or not exist in thread context,key={}", key);
            }
            return null;
        }

        value = resourceMap.get(key);
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieve resource from thread context,[key={},value={}]", key, value);
        }
        return value;
    }

    /**
     * UnBind resource for given key from current thread
     *
     * @param key
     * @return
     */
    public static Object remove(Object key) {
        Object value = null;
        Map<String, Object> resourceMap = resources.get();

        if (resourceMap == null || resourceMap.size() == 0 || !resourceMap.containsKey(key)) {
            if (logger.isDebugEnabled()) {
                logger.debug("The resource is already unbind or not exist in thread context,key={}", key);
            }
            return null;
        }

        value = resourceMap.remove(key);
        if (logger.isDebugEnabled()) {
            logger.debug("Remove resource from thread context,[key={},value={}]", key, value);
        }
        if (resourceMap.size() == 0) {
            resources.set(null);
        }

        return value;
    }

    /**
     * Check whether resource has binded to current thread
     *
     * @param key
     * @return
     */
    public static boolean exists(String key) {
        Map<String, Object> resourceMap = resources.get();
        if (resourceMap == null || resourceMap.size() == 0 || !resourceMap.containsKey(key)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Clean all resource for current thread
     */
    public static void removeAll() {
        resources.set(null);
        if (logger.isDebugEnabled()) {
            logger.debug("Clean all thread bind resouces successfully");
        }
    }


    public static AppUser getCurrentUser() {
        AppUser appUser = (AppUser) ThreadContext.get(ThreadContextConstant.CURRENT_USER);
        return appUser;
    }

    public static void setCurrentUser(AppUser appUser) {
        ThreadContext.set(ThreadContextConstant.CURRENT_USER, appUser);
        if (appUser != null) {
            ThreadContext.set(ThreadContextConstant.USERNAME, appUser.getUsername());
            MDC.put(ThreadContextConstant.USERNAME, appUser.getUsername());
            ThreadContext.set(ThreadContextConstant.USER_ID, appUser.getId());
        }
    }

    public static String getAccessToken() {
        return (String) get(ThreadContextConstant.ACCESS_TOKEN);
    }

    public static void setAccessToken(String accessToken) {
        set(ThreadContextConstant.ACCESS_TOKEN, accessToken);
    }

    public static String getCurrentUsername() {
        return (String) ThreadContext.get(ThreadContextConstant.USERNAME);
    }

    public static Long getCurrentUserId() {
        return (Long) ThreadContext.get(ThreadContextConstant.USER_ID);
    }

}
