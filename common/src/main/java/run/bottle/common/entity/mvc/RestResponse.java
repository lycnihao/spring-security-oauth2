package run.bottle.common.entity.mvc;

import org.springframework.http.HttpStatus;
import run.bottle.common.context.ThreadContext;

import java.io.Serializable;

/**
 * @author liyc
 * @date 2022-04-15
 */
public class RestResponse implements Serializable {

    /**
     * 请求是否成功
     */
    private Boolean success;

    /**
     * http 状态码
     */
    private Integer status;

    /**
     * 返回信息、异常信息
     */
    private String message;

    /**
     * 返回结果对象
     */
    private Object result;

    /**
     * 请求追踪号
     */
//    private String traceId;

    private String timestamp;

    private String accessToken;

    protected RestResponse() {
        this.setTimestamp(System.currentTimeMillis() + "");
        this.setAccessToken(ThreadContext.getAccessToken());
    }

    public static RestResponse ok() {
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(true);
        restResponse.setStatus(HttpStatus.OK.value());
        restResponse.setMessage("success");
        restResponse.setTimestamp(System.currentTimeMillis() + "");
        restResponse.setAccessToken(ThreadContext.getAccessToken());
        return restResponse;
    }

    public static RestResponse error() {
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(false);
        restResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        restResponse.setTimestamp(System.currentTimeMillis() + "");
        restResponse.setAccessToken(ThreadContext.getAccessToken());
        return restResponse;
    }

    public RestResponse result(Object data) {
        this.setResult(data);
        return this;
    }

    public RestResponse message(String message) {
        this.setMessage(message);
        return this;
    }

    public RestResponse status(Integer status) {
        this.setStatus(status);
        return this;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

//    public String getTraceId() {
//        return traceId;
//    }
//
//    public void setTraceId(String traceId) {
//        this.traceId = traceId;
//    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
