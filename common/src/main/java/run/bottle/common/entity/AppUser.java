package run.bottle.common.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author liyc
 * @date 2022-04-05
 */
@Getter
@Setter
public class AppUser implements Serializable {

    private Long id;
    private String username;
    private String name;
    private String phone;
    private String email;
    private String avatar;
    private String description;
    private String defaultLanguage;
    private List<Long> roleIds;
    private String token;
    private Boolean authResult;
    private String message;
    private Boolean status;

}
