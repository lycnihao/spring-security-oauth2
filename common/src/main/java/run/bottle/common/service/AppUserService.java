package run.bottle.common.service;

import run.bottle.common.entity.AppUser;

/**
 * @author liyc
 * @date 2022-04-12
 */
public interface AppUserService {

    AppUser getAppUserByUsername(String username);

}
