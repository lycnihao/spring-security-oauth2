package run.bottle.urp.service;

import run.bottle.urp.model.po.Role;

import java.util.List;

/**
 * @author liyc
 * @date 2022-03-25
 */
public interface RoleService {

    public Role saveRole(Role role);

    public List<Role> findRoleByUserId(Long userId);

    public List<Role> findByIdIn(List<Long> ids);

}
