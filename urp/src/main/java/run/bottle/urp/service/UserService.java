package run.bottle.urp.service;

import run.bottle.common.service.AppUserService;
import run.bottle.urp.model.po.Permission;
import run.bottle.urp.model.po.Role;
import run.bottle.urp.model.po.User;

import java.util.List;

/**
 * @author liyc
 * @date 2022-03-25
 */
public interface UserService extends AppUserService {

    public User findByUsername(String username);

    public User saveUser(User user);

    public User addRoleToUser(String username, String roleName);

}
