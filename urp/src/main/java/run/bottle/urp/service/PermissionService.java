package run.bottle.urp.service;

import run.bottle.urp.model.po.Permission;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author liyc
 * @date 2022-04-04
 */
public interface PermissionService {

    void save(Permission permission);

    void addPermissionToRole(String resourceId, String roleName);

    List<Permission> findByRoleIds(List<Long> roleIds);

    Map<Long, List<Permission>> findRolePermissionByRoleId(List<Long> roleIds);

}
