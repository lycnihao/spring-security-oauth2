package run.bottle.urp.model.vo;

import run.bottle.urp.model.po.Permission;
import run.bottle.urp.model.po.Role;

import java.util.List;
import java.util.Set;

/**
 * @author liyc
 * @date 2022-04-15
 */
public class UserInfoVo {

    private Long id;
    private String username;
    private String name;
    private String userSex;
    private String tenant;
    private String phone;
    private String email;
    private String avatar;
    private String description;
    private String defaultLanguage;
    private List<Role> role;
    private Set<Permission> resources;
    private Set<String> stringResources;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }

    public Set<Permission> getResources() {
        return resources;
    }

    public void setResources(Set<Permission> resources) {
        this.resources = resources;
    }

    public Set<String> getStringResources() {
        return stringResources;
    }

    public void setStringResources(Set<String> stringResources) {
        this.stringResources = stringResources;
    }
}
