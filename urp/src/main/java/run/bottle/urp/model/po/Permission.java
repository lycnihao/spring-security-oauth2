package run.bottle.urp.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import run.bottle.common.entity.BaseEntity;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author liyc
 * @date 2022-03-25
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_pub_permission")
public class Permission extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "id")
    private Long id;

    /**
     * 父权限
     */
    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "resource_key")
    private String resourceKey;

    @Column(name = "resource_name")
    private String resourceName;

    @Column(name = "resource_type")
    private String resourceType;

    @Column(name = "resource_icon")
    private String resourceIcon;

    @Column(name = "resource_method")
    private String resourceMethod;

    @Column(name = "resource_path")
    private String resourcePath;

    @Column(name = "resource_url")
    private String resourceUrl;

    @Column(name = "resource_order")
    private Integer resourceOrder;

    @Column(name = "resource_level")
    private Integer resourceLevel;

    @Column(name = "resource_show")
    private Boolean resourceShow = true;

    @Column(name = "resource_cache")
    private Boolean resourceCache = true;

    @Transient
    private Set<Permission> children = new LinkedHashSet<>();

    public Permission(Long id, Long parentId, String resourceKey, String resourceName, String resourceType, String resourceIcon, String resourceMethod, String resourcePath, String resourceUrl, Integer resourceOrder, Integer resourceLevel, Boolean resourceShow, Boolean resourceCache) {
        this.id = id;
        this.parentId = parentId;
        this.resourceKey = resourceKey;
        this.resourceName = resourceName;
        this.resourceType = resourceType;
        this.resourceIcon = resourceIcon;
        this.resourceMethod = resourceMethod;
        this.resourcePath = resourcePath;
        this.resourceUrl = resourceUrl;
        this.resourceOrder = resourceOrder;
        this.resourceLevel = resourceLevel;
        this.resourceShow = resourceShow;
        this.resourceCache = resourceCache;
    }
}
