package run.bottle.urp.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import run.bottle.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @author liyc
 * @date 2022-03-25
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_pub_user")
public class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "id")
    private Long id;

    /**
     * 用户名
     */
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    /**
     * 姓名
     */
    @Column(name = "name")
    private String name;

    @Column(name = "user_sex")
    private String userSex;

    /**
     * 注册手机号
     */
    @Column(name = "phone")
    private String phone;

    /**
     * 注册邮箱
     */
    @Column(name = "email")
    private String email;

    @Column(name = "description")
    private String description;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "status")
    private String status;

    @Column(name = "deleted")
    private Boolean deleted;



}
