package run.bottle.urp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import run.bottle.urp.model.po.User;

/**
 * @author liyc
 * @date 2022-03-25
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
