package run.bottle.urp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import run.bottle.urp.model.po.Permission;

import java.util.List;

/**
 * @author liyc
 * @date 2022-04-04
 */
@Repository
public interface PermissionRepo extends JpaRepository<Permission, Long> {

    List<Permission> findByIdIn(List<Long> ids);

    Permission findByResourceKey(String resourceId);

}
