package run.bottle.urp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import run.bottle.urp.model.po.RolePermission;

import java.util.List;

/**
 * @author liyc
 * @date 2022-04-04
 */
@Repository
public interface RolePermissionRepo extends JpaRepository<RolePermission, Long> {

    List<RolePermission> findByRoleId(Long roleId);

    List<RolePermission> findByRoleIdIn(List<Long> roleIds);

}
