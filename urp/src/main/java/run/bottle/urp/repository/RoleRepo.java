package run.bottle.urp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import run.bottle.urp.model.po.Role;

import java.util.List;

/**
 * @author liyc
 * @date 2022-03-25
 */
@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {

    List<Role> findByIdIn(List<Long> ids);

    Role findByRoleName(String roleName);

}
