package run.bottle.urp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import run.bottle.urp.model.po.UserRole;

import java.util.List;

/**
 * @author liyc
 * @date 2022-03-25
 */
@Repository
public interface UserRoleRepo extends JpaRepository<UserRole, Long> {

    public List<UserRole> findByUserId(Long userId);

    public UserRole findByUserIdAndRoleId(Long userId, Long roleId);

}
