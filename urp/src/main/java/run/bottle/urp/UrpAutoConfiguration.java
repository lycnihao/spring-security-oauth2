package run.bottle.urp;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author liyc
 * @date 2022-03-26
 */
@Configuration
@EntityScan("run.bottle.urp.model.po")
@EnableJpaRepositories(value = {"run.bottle.urp.repository"})
@ComponentScan({"run.bottle.urp.controller", "run.bottle.urp.service"})
public class UrpAutoConfiguration {
}
