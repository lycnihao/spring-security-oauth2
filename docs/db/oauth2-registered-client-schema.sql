CREATE TABLE oauth2_registered_client (
    id varchar(100) NOT NULL,
    client_id varchar(100) NOT NULL,
    client_id_issued_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    client_secret varchar(200) DEFAULT NULL,
    client_secret_expires_at timestamp DEFAULT NULL,
    client_name varchar(200) NOT NULL,
    client_authentication_methods varchar(1000) NOT NULL,
    authorization_grant_types varchar(1000) NOT NULL,
    redirect_uris varchar(1000) DEFAULT NULL,
    scopes varchar(1000) NOT NULL,
    client_settings varchar(2000) NOT NULL,
    token_settings varchar(2000) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO `oauth2_registered_client` VALUES ('2d8e1b52-6753-4a5a-8600-8c69594ebbea', 'login-client', '2022-04-16 00:45:22', '$2a$10$HYV.iX7pLADyMx9qRS2i7e8rzssJHqZXZ.FoMObshHE7Hc3hwta/2', NULL, '2d8e1b52-6753-4a5a-8600-8c69594ebbea', 'client_secret_basic', 'refresh_token,authorization_code', 'http://127.0.0.1:8080/login/oauth2/code/login-client,http://127.0.0.1:8080/authorized', 'openid,profile', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}');
INSERT INTO `oauth2_registered_client` VALUES ('404b870e-7222-4811-a8ab-32e36e3b9cef', 'messaging-client', '2022-04-16 00:45:22', '$2a$10$3FLOKYKDbxJCrWLUQLulYuUh1B8JJnczGbteFgScIfm60tRaB3lf2', NULL, '404b870e-7222-4811-a8ab-32e36e3b9cef', 'client_secret_basic', 'client_credentials', '', 'message:read,message:write', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":false}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}');
INSERT INTO `oauth2_registered_client` VALUES ('722fe711-c13d-402d-96f9-ec689d8729d9', 'user-client', '2022-04-16 00:45:22', '$2a$10$AgChnYVtE//UrWsR7yx3.ObTZSat6VkmXXEa2sSgZFQ/XJHabJ8QC', NULL, '722fe711-c13d-402d-96f9-ec689d8729d9', 'client_secret_basic', 'refresh_token,password', '', 'message:read,message:write', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":false}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}');
