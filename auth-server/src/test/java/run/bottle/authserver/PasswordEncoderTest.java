package run.bottle.authserver;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author liyc
 * @date 2022-03-26
 */
public class PasswordEncoderTest {

    private static BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    public static void main(String[] args) {
        test1();
    }

    private static void test1() {
        String encodedPassword = PASSWORD_ENCODER.encode("secret");

        System.out.println(encodedPassword);
    }

    private static void  test2() {
        String secret = "secret";
        boolean matches = PASSWORD_ENCODER.matches(secret, "$2a$10$bHrth9EDKft3RI.3EkbVJO8IQ2xIqKAs46iQScuMWfpGJQlsSaI/6");
        System.out.println(matches);
    }

    private static void  test3() {
        String presentedPassword = "$2a$10$KSZ6eKJ74b9GGAaDLVo0bOKfBkB99PZk5lTl/hsCBU83xB0XnbuL.";
        String password = "secret";
        boolean matches = PASSWORD_ENCODER.matches(presentedPassword, password);
        System.out.println(matches);
    }

}
